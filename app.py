#!/usr/bin/env python

"""Runs the API when started
"""

import logging

from api import create_app


if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app = create_app()
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-a', '--address', default="127.0.0.1", type=str, help="IP the server should bind to")
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    args = parser.parse_args()
    app = create_app()
    app.run(host=args.address, port=args.port, debug=True)
