# TODO: Multistage: build wheels in first stage, install in second

# apt update && apt install libpq-dev postgres-client build-essential
# pip wheel --no-cache-dir --wheel-dir /opt/wheels -r requirements.txt

FROM python:3.12.0-slim-bookworm

WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY . .
RUN pip install -r requirements.txt && \
        chmod +x entrypoint.sh

# TODO: stop running as root

ENTRYPOINT ["./entrypoint.sh"]
