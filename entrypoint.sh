#!/bin/bash

function shutdown {
    kill -TERM "$gunicorn_pid"
    wait "$gunicorn_pid"
    exit
}

trap "shutdown" SIGTERM SIGKILL

if [ $# -eq 0 ]; then
    flask admins init
    gunicorn -w 4 -t 8 -b 0.0.0.0:8080 --log-level $CRUSHER_LOGLEVEL --worker-class gevent app:app &
    gunicorn_pid=$!
    wait $gunicorn_pid
else
    exec "$@"
fi