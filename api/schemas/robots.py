from flasgger import Schema
from marshmallow import fields, validate


class RobotsCreationSchema(Schema):
    class Meta:
        fields = ("token", "roles", "name", "owner")

    token = fields.Str(dump_only=True)
    roles = fields.List(fields.Str(), required=True, validate=validate.Length(min=1))
    name = fields.Str(required=True, validate=validate.Length(min=1))
    owner = fields.Str(required=True, validate=validate.Length(min=1))
