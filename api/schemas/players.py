from flasgger import Schema
from marshmallow import fields


class PlayerStatsSchema(Schema):
    name = fields.String(required=True)
    total_kills = fields.Integer(required=True)
    total_deaths = fields.Integer(required=True)
    total_score = fields.Integer(required=True)
    total_games = fields.Integer(required=True)
