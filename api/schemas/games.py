from datetime import datetime

from flasgger import Schema
from marshmallow import fields, validate


# Hack to let marshmallow know datetime objects are, in fact, datetimes
# https://github.com/marshmallow-code/marshmallow/issues/656#issuecomment-318587611
class ActualDatetime(fields.DateTime):
    def _deserialize(self, value, attr, data):
        if isinstance(value, datetime):
            return value
        return super()._deserialize(value, attr, data)


class PlayerSchema(Schema):
    bot = fields.Boolean(required=True)
    deaths = fields.Integer(required=True)
    kills = fields.Integer(required=True)
    name = fields.String(required=True)
    score = fields.Integer(required=True)


class TeamSchema(Schema):
    players = fields.List(fields.Nested(PlayerSchema), required=True, validate=validate.Length(min=1))


class GameSchema(Schema):
    class Meta:
        fields = ("_id", "timestamp", "server_name", "modded", "gamemode", "map", "teams")

    _id = fields.Int(dump_only=True)
    server_name = fields.Int(dump_only=True)
    timestamp = ActualDatetime(required=True, format="%Y-%m-%dT%H:%M:%S%z")
    modded = fields.Bool(required=True)
    gamemode = fields.Str(required=True, validate=validate.Length(min=1))
    map = fields.Str(required=True, validate=validate.Length(min=1))
    teams = fields.List(fields.Nested(TeamSchema), required=True, validate=validate.Length(min=1))


class LastQuerySchema(Schema):
    class Meta:
        fields = ("player", "server")

    player = fields.Str(required=False)
    server = fields.Str(required=False)
