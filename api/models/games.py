class GameModel:
    def __init__(self, timestamp, server_name, modded, gamemode, map, teams):
        self.timestamp = timestamp
        self.server_name = server_name
        self.modded = modded
        self.gamemode = gamemode
        self.map = map
        self.teams = teams
