"""Crusher kickin' in
"""

import logging

from flask import Flask

from .routes.admins import admins

from .routes.robots import robots

from .routes.games import games
from .routes.players import players

# from .routes.claims import claims

api_version = "v1"


def create_app():
    application = Flask(__name__)
    application.config.from_pyfile('config.py')

    FORMAT = "%(asctime)s %(levelname)s [%(name)s.%(funcName)s] %(message)s"
    logging.basicConfig(format=FORMAT, level=logging.getLevelName(application.config['LOGLEVEL'].upper()), datefmt="%Y-%m-%dT%H:%M:%S%z")

    if not application.config['MONGO_URI']:
        raise ValueError("No MongoDB URI was passed to the application. Did you define the env var CRUSHER_MONGO_URI?")

    if application.config['DEBUG']:
        from flasgger import Swagger

        application.config['SWAGGER'] = {
            'uiversion': 3,
            'persistAuthorization': True,
            "info": {
                "title": "Crusher - a Blacklight: Revive Data API",
                "description": "Ingests data from game servers to present it to external clients such as Discord bots",
            },
        }
        Swagger(application)

    application.register_blueprint(admins, url_prefix=f'/api/{api_version}/admins')
    application.register_blueprint(robots, url_prefix=f'/api/{api_version}/robots')
    application.register_blueprint(games, url_prefix=f'/api/{api_version}/games')
    application.register_blueprint(players, url_prefix=f'/api/{api_version}/players')
    # application.register_blueprint(claims, url_prefix=f'/api/{api_version}/claims')

    return application
