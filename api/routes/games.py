import logging

from flask import Blueprint, request, jsonify
from marshmallow.exceptions import ValidationError
from flasgger import swag_from

from ..utils.authentication import auth
from ..schemas.games import GameSchema, LastQuerySchema

from api.services.games import create_game_service, get_last_game_service


logger = logging.getLogger(__name__)
games = Blueprint('games', __name__)


@games.route("/", methods=['POST'])
@auth.login_required(role='server')
@swag_from({
    'requestBody': {
        'description': 'Games creation parameters',
        'required': True,
        'content': {
            'application/json': {
                'schema': GameSchema,
            },
        },
    },
    'responses': {
        201: {
            'description': 'New game entry created successfully.'
        },
        422: {
            'description': 'Invalid input data.'
        }
    }
})
def create_game():
    """Create a new game entry in the database.
    Some data (a timestamp and the server's name) are added to the document.
    ---
    """
    try:
        data = request.get_json()
        serialized_data = GameSchema().load(data)
    except ValidationError as err:
        return jsonify({"error": err.messages}), 422

    if create_game_service(serialized_data, auth.current_user()):
        return jsonify({"message": "Game created successfully"}), 201


@games.route("/", methods=['GET'])
@auth.login_required(role='admins')
def get_games():
    """

    ---
    """
    return jsonify({"error": "Not yet implemented"}), 501
    # return get_game_service()


@games.route("/last", methods=['GET'])
@auth.login_required(role='bot')
@swag_from({
    'responses': {
        200: {
            'description': 'Game found',
            'content': {
                'application/json': {
                    'schema': GameSchema,
                },
            },
        },
        404: {
            'description': 'No game found'
        }
    }
})
def get_last_game():
    """Gets the latest game that was ingested
    ---
    """
    query_params = LastQuerySchema().load(request.args)
    # NOTE: server is currently ignored if player is set
    game = get_last_game_service(query_params.get('player'), query_params.get('server'))

    if game is None:
        return jsonify({"error": "No game found"}), 404
    else:
        # load & dump to validate then apply proper datetime format
        print(game)
        data = GameSchema().load(game, unknown='exclude')
        return GameSchema().dump(data)
