import os
import logging

from flask import Blueprint, current_app

from ..services.admins import ensure_admin_user_exists

# from api.services.admins import get_admins


admins = Blueprint('admins', __name__)
logger = logging.getLogger(__name__)


@admins.cli.command("init")
def create_initial_admin_accounts() -> None:
    """Creates an administrator user in the database if it doesn't exist

    ---
    """
    with current_app._get_current_object().app_context():
        ensure_admin_user_exists(os.getenv('CRUSHER_INIT_ADMIN_USERNAME'), os.getenv('CRUSHER_INIT_ADMIN_PASSWORD'))
        logger.debug("Initialization complete.")
