from flask import Blueprint, request, jsonify
from marshmallow.exceptions import ValidationError
from pymongo.errors import DuplicateKeyError
from flasgger import swag_from

from ..utils.authentication import auth
from ..schemas.robots import RobotsCreationSchema

from api.services.robots import create_robot_service  # , get_robot_service, reset_robot_service, delete_robot_service


robots = Blueprint('robots', __name__)


@robots.route("/", methods=['POST'])
@auth.login_required(role='admin')
@swag_from({
    'requestBody': {
        'description': 'Server creation parameters',
        'required': True,
        'content': {
            'application/json': {
                'schema': RobotsCreationSchema,
            },
        },
    },
    'responses': {
        201: {
            'description': 'Robot created successfully.'
        },
        400: {
            'description': 'Robot with identical "name" already exists.'
        },
        422: {
            'description': 'Invalid input data.'
        }
    }
})
def create_robot():
    """Create a new "robot" type of user
    Unlike regular users, these are authenticated using a token.
    ---
    """
    try:
        data = request.get_json()
        serialized_data = RobotsCreationSchema().load(data)
    except ValidationError as err:
        return jsonify({"error": err.messages}), 422
    try:
        return jsonify({"message": "Robot created successfully", "object": create_robot_service(serialized_data)}), 201
    except DuplicateKeyError:
        return jsonify({"error": "Robot with identical 'name' already exists"}), 400


@robots.route("/", methods=['GET'])
@auth.login_required(role='admin')
def get_robots():
    """
    Gets every robot in the database. Admin-specific endpoint.
    ---
    """
    return jsonify({"error": "Not yet implemented"}), 501


@robots.route("/<robot_id>", methods=['GET'])
@auth.login_required(role='admin')
def get_robot_with_id(robot_id: int):
    """
    ---
    """
    return jsonify({"error": "Not yet implemented"}), 501


@robots.route("/<owner>", methods=['GET'])
@auth.login_required(role='admin')
def get_robots_of_owner(owner: str):
    """
    ---
    """
    return jsonify({"error": "Not yet implemented"}), 501


@robots.route("/<robot_id>", methods=['PATCH'])
@auth.login_required(role='admin')
def reset_robot_with_id(robot_id: int):
    """
    ---
    """
    return jsonify({"error": "Not yet implemented"}), 501


@robots.route("/<robot_id>", methods=['DELETE'])
@auth.login_required(role='admin')
def delete_robot_with_name(robot_id: int):
    """
    ---
    """
    return jsonify({"error": "Not yet implemented"}), 501
