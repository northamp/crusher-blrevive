import logging

from flask import Blueprint, jsonify
from flasgger import swag_from

from ..utils.authentication import auth
from ..schemas.players import PlayerStatsSchema

from api.services.players import get_player_one_service


logger = logging.getLogger(__name__)
players = Blueprint('players', __name__)


@players.route("/<player_name>", methods=['GET'])
@auth.login_required(role='bot')
@swag_from({
    'responses': {
        200: {
            'description': 'Player found',
            'content': {
                'application/json': {
                    'schema': PlayerStatsSchema,
                },
            },
        },
        404: {
            'description': 'No player found'
        }
    }
})
def get_player_with_name(player_name: str):
    """Gets a specific player's lifetime statistics
    ---
    """
    player = get_player_one_service(player_name)
    if player is None:
        return jsonify({"error": "No player found"}), 404
    else:
        return PlayerStatsSchema().load(get_player_one_service(player_name), unknown='exclude')
