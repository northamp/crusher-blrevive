# from flask import Blueprint, request

# from ..utils.authentication import auth

# from api.services.games import create_claim_service, get_claim_service, modify_claim_service, delete_claim_service


# claims = Blueprint('claims', __name__)


# @claims.route("/", methods=['POST'])
# @auth.login_required(role='bots')
# def create_claim():
#     """
#     ---
#     """
#     post_data = request.get_json()
#     return create_claim_service(post_data)


# @claims.route("/", methods=['GET'])
# @auth.login_required(role='admins')
# def get_claims():
#     """
#     ---
#     """
#     return get_claim_service()


# @claims.route("/<discord_id>", methods=['GET'])
# @auth.login_required(role='bots')
# def get_claim_with_discord_id(discord_id: int):
#     """
#     ---
#     """
#     return get_claim_service(id=discord_id)


# @claims.route("/<discord_id>", methods=['PATCH'])
# @auth.login_required(role='bots')
# def modify_claim_with_discord_id(discord_id: int):
#     """
#     ---
#     """
#     return modify_claim_service(id=discord_id)


# @claims.route("/<discord_id>", methods=['DELETE'])
# @auth.login_required(role='bots')
# def delete_claim_with_discord_id(discord_id: int):
#     """
#     ---
#     """
#     return delete_claim_service(id=discord_id)
