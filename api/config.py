"""Load configuration from env vars
"""

import os

DEBUG = os.environ.get('CRUSHER_DEBUG', False)
LOGLEVEL = os.environ.get('CRUSHER_LOGLEVEL', "info")

MONGO_URI = os.getenv('CRUSHER_MONGO_URI')

ADMIN_USERNAME = os.getenv('CRUSHER_INIT_ADMIN_USERNAME')
ADMIN_PASSWORD = os.getenv('CRUSHER_INIT_ADMIN_PASSWORD')
