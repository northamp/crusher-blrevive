import logging

from ..utils.mdb import db
from ..utils.authentication import hash_password

logger = logging.getLogger(__name__)


def ensure_admin_user_exists(admin_username: str, admin_password: str):
    """Takes an username/password combo and creates the appropriate entry in the database if the username isn't already there.

    Parameters
    -----------
    admin_username: str
        Admin username to check/create
    admin_password: str
        Admin password to use if no entry by that name exists
    """
    if not admin_username:
        logger.debug("No admin_username specified, skipping init")
        return False
    else:
        admin_credentials = {"username": admin_username}
        admin_user = db.users.find_one(admin_credentials)
        if admin_user is None:
            hashed_password = hash_password(admin_password)
            admin_data = {
                "username": admin_username,
                "password": hashed_password,
                "roles": ["admin"]
            }
            db.users.insert_one(admin_data)
            logger.debug(f'Created an admin account named "{admin_username}"')
            return True
        else:
            logger.debug(f'An admin user by the name of "{admin_username}" already exists, skipping creation')
