import logging

from ..utils.mdb import db
from ..models.robots import RobotsModel
from ..utils.authentication import hash_password, generate_token


logger = logging.getLogger(__name__)


def create_robot_service(serialized_data):
    """Creates a new robot account. Roles depend on the type."""
    clear_token = generate_token()
    token = hash_password(clear_token)
    document = RobotsModel(
        name=serialized_data['name'],
        owner=serialized_data['owner'],
        roles=serialized_data['roles'],
        token=token
    )

    db.robots.create_index("name", unique=True)

    db.robots.insert_one(document.__dict__)
    # clean-up before return
    document.token = clear_token
    document.__dict__.pop('_id', None)
    return document.__dict__
