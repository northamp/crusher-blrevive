import logging

from ..utils.mdb import db
from ..models.games import GameModel


logger = logging.getLogger(__name__)


def create_game_service(serialized_data, server):
    """Creates a new game entry"""
    document = GameModel(
        timestamp=serialized_data["timestamp"],
        server_name=server["name"],
        modded=serialized_data["modded"],
        gamemode=serialized_data["gamemode"],
        map=serialized_data["map"],
        teams=serialized_data["teams"]
    )

    db.games.insert_one(document.__dict__)
    document.__dict__.pop('_id', None)

    return True


def get_last_game_service(player: str = None, server: str = None):
    """Gets the latest game a specific player was in, or for a specific server"""
    # NOTE: server is currently ignored if player is set
    if player:
        return db.games.find_one({"teams.players.name": player}, sort=[("timestamp", -1)])
    if server:
        return db.games.find_one({"server_name": server}, sort=[("timestamp", -1)])
    else:
        return db.games.find_one({}, sort=[("timestamp", -1)])
