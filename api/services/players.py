import logging

from ..utils.mdb import db


logger = logging.getLogger(__name__)


def get_player_one_service(player_name: str = None):
    """Gets a single player's total stats"""
    pipeline = [
        {"$unwind": "$teams"},
        {"$unwind": "$teams.players"},
        {"$match": {"teams.players.name": player_name}},
        {"$group": {
            "_id": "$teams.players.name",
            "total_kills": {"$sum": {"$getField": {"field": "kills", "input": "$teams.players"}}},
            "total_deaths": {"$sum": {"$getField": {"field": "deaths", "input": "$teams.players"}}},
            "total_score": {"$sum": {"$getField": {"field": "score", "input": "$teams.players"}}},
            "total_games": {"$sum": {"$getField": {"field": "games", "input": "$teams.players"}}}
        }}
    ]

    try:
        pipe_result = db.games.aggregate(pipeline)
        result = pipe_result.next()
        result['name'] = player_name
        return result
    except StopIteration:
        return None
