import logging
import uuid

from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth, MultiAuth
from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError

from .mdb import db


logger = logging.getLogger(__name__)

password_hasher = PasswordHasher()

basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth('Bearer')
auth = MultiAuth(basic_auth, token_auth)


def hash_password(password: str) -> str:
    """
    Hashes a password using Argon2

    Parameters
    -----------
    password: str
        String that should be hashed
    """
    return password_hasher.hash(password)


def verify_password(hashed_password: str, provided_password: str) -> str:
    """
    Checks password validity using Argon2

    Parameters
    -----------
    provided_password: str
        String that should be checked
    hashed_password: str
        Argon2-hashed string that provided_password should be compared to
    """
    try:
        password_hasher.verify(hashed_password, provided_password)
        return True
    except VerifyMismatchError:
        return False


def generate_token() -> str:
    """
    Creates a token
    """
    return str(uuid.uuid4())


@basic_auth.verify_password
def verify_user_password(username: str, password: str):
    if username == "":  # Not 'is None' since an empty string might be provided
        return
    user = db.users.find_one({"username": username})
    if user is None:
        return
    else:
        if verify_password(user["password"], password):
            return user


@token_auth.verify_token
def verify_token(token):
    if token == "":  # Not 'is None' since an empty string might be provided
        return
    all_robots = db.robots.find({})
    if all_robots is None:
        return
    # Iterate over each documents and compare hashed tokens
    # Could very likely become sluggish if lots of robots were there
    # but who am I kiddin, like 2 instances of that thing would ever run
    # (my own staging and production instances that is)
    for robot in all_robots:
        if verify_password(robot['token'], token):
            return robot
    # If loop completes without finding a matching bot
    return


# Doesn't seem like there's a way to use get_user_roles with MultiAuth unfortunately
def get_user_roles(user):
    return user['roles']


@basic_auth.get_user_roles
def basic_get_user_roles(user):
    return get_user_roles(user)


@token_auth.get_user_roles
def token_get_user_roles(user):
    return get_user_roles(user)
