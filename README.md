# Crusher - A BL:Revive statistics RESTful API

Companion project of the [stats-uploader](https://gitlab.com/northamp/stats-uploader) module, this Python API stores data sent by the game using this module.

[Deacon](https://gitlab.com/northamp/deacon-blrevive) also has a Cog that allows displaying the collected stats.

It has the following goals:

* Provide a way to simplify ingesting game data into a database, leaving as less tasks as possible to the game server itself
* Provide a secure, controlled way to access the data from external clients (such as Discord)

## Configuration

| Envvar                        | Description                                                                   | Default value |
| ----------------------------- | ----------------------------------------------------------------------------- | ------------- |
| `CRUSHER_INIT_ADMIN_USERNAME` | Creates a new admin user if one doesn't already exists with the given name    | `crusher`     |
| `CRUSHER_INIT_ADMIN_PASSWORD` | Creates a new Superuser if one doesn't already exists with the given password | `crusher`     |
| `CRUSHER_DEBUG`               | Enable Flask's debug mode and Swagger                                         | `False`       |
| `CRUSHER_LOGLEVEL`            | Determines logs verbosity                                                     | `info`        |
| `CRUSHER_MONGO_URI`           | MongoDB connection string (including credentials & database)                  | None          |

## Architecture

*PlantUML diagram, contains link if exported as SVG!*

```plantuml
@startuml Statistics collection system for Blacklight: REvive

title Statistics collection system for Blacklight: Revive

!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons
!define FONTAWESOME https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/font-awesome-5
!include FONTAWESOME/users.puml
!include FONTAWESOME/gamepad.puml
!include FONTAWESOME/discord.puml
!include DEVICONS/python.puml
!include DEVICONS/mongodb.puml

HIDE_STEREOTYPE()

AddRelTag("bot", $lineStyle = DashedLine())

Container(blre, "Blacklight: Revive Server", "C++", "Blacklight: Revive game server running the stats-uploader module", $sprite="gamepad", $link="https://gitlab.com/northamp/docker-blrevive")

Container(crusher, "Crusher (API)", "python", "Flask API that handles receiving, storing and exposing game data", $sprite="python", $link="https://gitlab.com/northamp/crusher-blrevive")
ContainerDb(db, "Database", "MongoDB", "Stores game data", $sprite="mongodb")

Container(deacon, "Deacon", "python", "Discord bot\nrunning the stats cog", $sprite="python", "https://gitlab.com/northamp/deacon-blrevive")

Person(user, "Players", "Blacklight: Revive players", $sprite="users")
Container_Ext(discord, "Discord", "N/A", "Lets people pay 10 currency units a month for 32x32px memes", $sprite="discord")

Rel_R(blre, crusher, "POSTs game data\nwhen round ends", "https")
Rel_R(crusher, db, "Reads/Writes")

Rel_U(deacon, crusher, "Polls the API", "https")
Rel_R(deacon, discord, "Replies to commands", "https")

Rel_U(user, discord, "", "")
Rel_U(user, deacon, "", "", $tags="bot")

@enduml
```

## (planned) Features

* Ingest stats-uploader data
  * Authentify servers through a simple (server-specific) API token for the time being
* Expose that data using structured queries accessed through various API calls
  * *Public or not?*

## In the future

* Let players claim their names through Discord commands
